'use strict'

//cargar moduos
var express = require('express');
var bodyParser = require('body-parser');

//ejecutar express
var app= express();

//cacgar ficheros
var producto_routes = require('./routes/producto');
var cliente_routes = require('./routes/cliente');
var categoria_routes = require('./routes/categoria');
var comprobante_routes = require('./routes/comprobante');
var ventas_routes = require('./routes/ventas');
var cuentas_por_cobrar = require('./routes/cuentas-por-cobrar');
var tipo_recibo = require('./routes/tipo-recibo');

//cargar midleweare
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

//añadir prefijos a rutas
app.use('/api',producto_routes);
app.use('/api',cliente_routes);
app.use('/api',categoria_routes);
app.use('/api',comprobante_routes);
app.use('/api',ventas_routes);
app.use('/api',cuentas_por_cobrar);
app.use('/api',tipo_recibo);

//exportar el modulo
module.exports = app;