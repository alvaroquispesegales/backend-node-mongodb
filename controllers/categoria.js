'use strict'
var validator = require('validator');
var fs = require('fs');
var path = require('path');
var Categoria = require('../models/categoria');

var controller = {

    guardarCategoria: (req, res) => {

        var params = req.body
        try {
            var validate_nombre = !validator.isEmpty(params.nombre);
            var validate_nota = !validator.isEmpty(params.nota);
        } catch (err) {

            return res.status(200).send({
                status: 'error',
                menssaje: 'fatan datos por enviar'
            });
        }
        if (validate_nombre && validate_nota) {

            //asignar valores
            var categoria = new Categoria();
            categoria.nombre = params.nombre;
            categoria.nota = params.nota;

            //guardar el articulo
            categoria.save((err, categoriaStore) => {

                if (err || !categoriaStore) {
                    setTimeout(function () {
                        return res.status(200).send({
                            status: 'error',
                            menssaje: 'la categoria no se ha guardado'
                        })
                    }, 2000);
                }
                setTimeout(function () {
                    return res.status(200).send({
                        status: 'success',
                        categoriaStore
                    })
                }, 2000);

            });

        } else {
            return res.status(200).send({
                status: 'error',
                menssaje: 'datos no validos'
            })
        }
    },

    obtenerCategorias: (req, res) => {
        Categoria.find({}).sort('-_id').exec((err, categorias) => {
            if (err) {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'errro al devolver los datos'
                })

            }
            if (!categorias) {

                return res.status(200).send({
                    status: 'error',
                    menssaje: 'no hay categorias para mostrar'
                })

            }

            return res.status(200).send({
                status: 'success',
                categorias
            })

        })

    },
    actualizarCategoria: (req, res) => {
        var categoriaId = req.params.id;
        var params = req.body
        try {
            var validate_nombre = !validator.isEmpty(params.nombre);
            var validate_nota = !validator.isEmpty(params.nota);
        }
        catch (err) {
        
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'Faltan datos por enviar' + err
                })
         
        }

        if (validate_nombre && validate_nota) {
            Categoria.findByIdAndUpdate({ _id: categoriaId }, params, { new: true }, (err, categoriaUpdate) => {
                if (err) {


                    setTimeout(function () {
                        return res.status(200).send({
                            status: 'error',
                            menssaje: 'Error al actualizar: ' + err
                        })
                    }, 2000);
                }
                if (!categoriaUpdate) {
                    setTimeout(function () {
                        return res.status(200).send({
                            status: 'error',
                            menssaje: 'No existe el cliente'
                        })
                    }, 2000);
                }

                setTimeout(function () {
                    return res.status(200).send({
                        status: 'success',
                        categoria: categoriaUpdate
                    })
                }, 2000);
            })
        } else {
            setTimeout(function () {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'La validacion no es correcta, Mensaje desde Servicio'
                })
            }, 2000);
        }
    },
    eliminarCategoria: (req, res) => {
        var categoriaId = req.params.id;
        Categoria.findOneAndDelete({ _id: categoriaId }, (err, categoriaRemoved) => {
            if (err) {


                setTimeout(function () {
                    return res.status(200).send({
                        status: 'error',
                        menssaje: 'error al borrar'
                    })
                }, 2000);
            }
            if (!categoriaRemoved) {


                setTimeout(function () {
                    return res.status(200).send({
                        status: 'error',
                        menssaje: 'No se ha borrado el producto, posiblemente no exista'
                    })
                }, 2000);
            }


            setTimeout(function () {
                return res.status(200).send({
                    status: 'success',
                    categoria: categoriaRemoved
                })
            }, 2000);
        })
    }
};

module.exports = controller