'use strict'
var validator = require('validator');
var Cliente = require('../models/cliente');
var controller = {

    guardarCliente: (req, res) => {

        var params = req.body
        try {

            var validate_identificacion = !validator.isEmpty(params.identificacion);
            var validate_nombre = !validator.isEmpty(params.nombre_completo);
            var validate_correo = !validator.isEmpty(params.correo);
            var validate_telefono = !validator.isEmpty(params.telefono);
            var validate_direccion = !validator.isEmpty(params.direccion);
            var validate_nota = !validator.isEmpty(params.nota);
        } catch (err) {

            return res.status(200).send({
                status: 'error',
                menssaje: 'fatan datos por enviar'
            })

        }
        if (validate_nombre && validate_correo &&
            validate_telefono && validate_direccion &&
            validate_identificacion && validate_nota) {

            var objCliente = new Cliente();
            objCliente.identificacion = params.identificacion;
            objCliente.nombre_completo = params.nombre_completo;
            objCliente.correo = params.correo;
            objCliente.telefono = params.telefono;
            objCliente.direccion = params.direccion;
            objCliente.nota = params.nota;

            objCliente.save((err, cliente) => {
                if (err || !cliente) {
                    setTimeout(function () {
                        return res.status(200).send({
                            status: 'error',
                            menssaje: 'no se ha podido error'
                        })
                    }, 2000);

                }
                setTimeout(function () {
                    return res.status(200).send({
                        status: "success",
                        cliente
                    })
                }, 2000);
            })
        } else {

            return res.status(200).send({
                error: 'error',
                menssaje: 'los datos no son validos'
            })

        }
    },
    obtenerClientes: (req, res) => {
        var query = Cliente.find({});
        var last = req.params.last;
        if (last || last != undefined) {
            query.limit(5);
        }
        query.sort('-_id').exec((err, clientes) => {
            if (err) {

                return res.status(200).send({
                    status: 'error',
                    menssaje: 'errro al devolver los datos'
                })

            }
            if (!clientes) {

                return res.status(200).send({
                    status: 'error',
                    menssaje: 'no hay clientes para mostrar'
                })


            }

            return res.status(200).send({
                error: 'success',
                clientes
            })

        })
    },
    obtenerCliente: (req, res) => {
        var clienteId = req.params.id;
        if (!clienteId && clienteId == null) {



            return res.status(200).send({
                status: 'error',
                menssaje: 'No exisate el cliente'
            })

        }

        Cliente.findById(clienteId, (err, cliente) => {

            if (err || !cliente) {


                return res.status(200).send({
                    status: 'error',
                    menssaje: 'No exisate el cliente'
                })


            }


            return res.status(200).send({
                status: 'success',
                cliente
            })

        })
    },
    actualizarCliente: (req, res) => {
        var clienteId = req.params.id;
        var params = req.body
        try {
            var validate_identificacion = !validator.isEmpty(params.identificacion);
            var validate_nombre = !validator.isEmpty(params.nombre_completo);
            var validate_correo = !validator.isEmpty(params.correo);
            var validate_telefono = !validator.isEmpty(params.telefono);
            var validate_direccion = !validator.isEmpty(params.direccion);
            var validate_nota = !validator.isEmpty(params.nota);

        }
        catch (err) {

            return res.status(200).send({
                status: 'error',
                menssaje: 'Faltan datos por enviar' + err
            })

        }

        if (validate_nombre && validate_correo && validate_telefono && validate_direccion &&
            validate_identificacion && validate_nota) {
            Cliente.findByIdAndUpdate({ _id: clienteId }, params, { new: true }, (err, clienteUpdate) => {
                if (err) {
                    setTimeout(function () {
                        return res.status(500).send({
                            status: 'error',
                            menssaje: 'Error al actualizar: ' + err
                        })
                    }, 2000);
                }
                if (!clienteUpdate) {
                    setTimeout(function () {
                        return res.status(200).send({
                            status: 'error',
                            menssaje: 'No existe el cliente'
                        })
                    }, 2000);
                }
                setTimeout(function () {
                    return res.status(200).send({
                        status: 'success',
                        cliente: clienteUpdate
                    })
                }, 2000);
            })
        } else {
            setTimeout(function () {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'La validacion no es correcta'
                })
            }, 2000);
        }
    },
    eliminarCliente: (req, res) => {
        var clienteId = req.params.id;
        Cliente.findOneAndDelete({ _id: clienteId }, (err, clienteRemoved) => {
            if (err) {

                setTimeout(function () {
                    return res.status(200).send({
                        status: 'error',
                        menssaje: 'error al borrar'
                    })
                }, 2000);
            }
            if (!clienteRemoved) {

                setTimeout(function () {
                    return res.status(200).send({
                        status: 'error',
                        menssaje: 'No se ha borrado el cliente, posiblemente no exista'
                    })
                }, 2000);
            }

            setTimeout(function () {
                return res.status(200).send({
                    status: 'success',
                    cliente: clienteRemoved
                })
            }, 2000);
        })
    }

};

module.exports = controller