'use strict'
var Comprobante = require('../models/comprobante');
var controller= {
    obtenerComprobantes:(req,res)=>{
        var query = Comprobante.find({});
        query.sort('-_id').exec((err,comprobantes)=>{
            if(err){
                return res.status(200).send({
                    status:'error',
                    menssaje : 'errro al devolver los datos'
                })
            }
            if(!comprobantes){
                return res.status(200).send({
                    status:'error',
                    menssaje : 'no hay comprobante para mostrar'
                })
            }
            return res.status(200).send({
                error:'success',
                comprobantes
            })
        })
    },
}
module.exports = controller