'use strict'
var validator = require('validator');
var fs = require('fs');
var path = require('path');
var VentaCabecera = require('../../models/ventaCabecera');
var VentaDetalle = require('../../models/ventaDetalle');

var controller = {
    obtenerCuentaCorriente: (req, res) => {
        VentaCabecera.find({ cliente: req.params.id_cliente }).populate('comprobante').sort('-_id').exec((err, venta_cab) => {
            if (err) {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'errro al devolver los datos'
                })
            }

            var cuenta_corriente = venta_cab.map(function (obj) {
                var rObj = {
                    nro_comprobante: obj.nro_venta,
                    fecha_venta: obj.fecha_venta,
                    tipo_comprobante_descripcion: obj.comprobante.nombre,
                    total_venta: 12,
                    total_parcial:0
                };
                return rObj;
            });


            return res.status(200).send({
                status: 'success',
                cuenta_corriente
            })

        })

    },
};
module.exports = controller