'use strict'
var validator = require('validator');
var ReciboCabecera = require('../../models/reciboCabecera');
var controller = {
    guardarReciboCabecera: (req, res) => {
        var params = req.body
        try {
            var validate_nro_recibo = !validator.isEmpty(params.nro_recibo);
            var validate_cliente = !validator.isEmpty(params.cliente);
            var validate_fecha_recibo = !validator.isEmpty(params.fecha_recibo);
            var validate_nota = !validator.isEmpty(params.nota);
        } catch (err) {
            return res.status(200).send({
                status: 'error',
                menssaje: 'fatan datos por enviar'
            })
        }
        if(!validate_nro_recibo || !validate_cliente || !validate_fecha_recibo || !validate_nota){
            return res.status(200).send({
                error: 'error',
                menssaje: 'los datos no son validos'
            })
        }
        //asignar valores
        var reciboCab = new ReciboCabecera();
        reciboCab.nro_recibo = params.nro_recibo;
        reciboCab.cliente = params.cliente;
        reciboCab.fecha_recibo = params.fecha_recibo;
        reciboCab.nota = params.nota;
        //guardar la venta cabecera
        reciboCab.save((err, reciboCabStore) => {
            if (err || !reciboCabStore) {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'Recibo no se ha guardado'
                })
            }
            setTimeout(function () {
                return res.status(200).send({
                    status: 'success',
                    reciboCabecera: reciboCabStore
                })
            }, 2000);

        });

    },

    actualizarReciboCabecera: (req, res) => {
        var id_cabecera = req.params.id_cabecera;
        var params = req.body
        ReciboCabecera.findByIdAndUpdate({ _id: id_cabecera }, params, { new: true }, (err, reciboCabUpdate) => {
            if (err) {
                setTimeout(function () {
                    return res.status(500).send({
                        status: 'error',
                        menssaje: 'Error al actualizar: ' + err
                    })
                }, 2000);
            }
            if (!reciboCabUpdate) {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'No existe el Recibo'
                })
            }

            return res.status(200).send({
                status: 'success',
                reciboCabecera: reciboCabUpdate
            })

        })

    }
}

module.exports = controller