'use strict'
var validator = require('validator');
var ReciboDetalle = require('../../models/reciboDetalle');

var controller = {

    guardarReciboDetalle: (req, res) => {
        var params = req.body
        var reciboDet = new ReciboDetalle();
        reciboDet.recibo_cabecera =  params.recibo_cabecera;
        reciboDet.tipo_recibo = params.tipo_recibo;
        reciboDet.nota = params.nota;
        reciboDet.valor = params.valor;

        reciboDet.save((err, reciboDetStore) => {
            if (err || !reciboDetStore) {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'Recibo no se ha guardado'
                })
            }

            return res.status(200).send({
                status: 'success',
                reciboDetalle: reciboDetStore
            })

        });

    },

    actualizarReciboDetalle: (req, res) => {
        var id_detalle = req.params.id_detalle;
        var params = req.body
        ReciboDetalle.findByIdAndUpdate({ _id: id_detalle }, params, { new: true }, (err, reciboDetUpdate) => {
            if (err) {
                setTimeout(function () {
                    return res.status(500).send({
                        status: 'error',
                        menssaje: 'Error al actualizar: ' + err
                    })
                }, 2000);
            }
            if (!reciboDetUpdate) {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'No existe el Recibo'
                })
            }

            return res.status(200).send({
                status: 'success',
                reciboDetalle: reciboDetUpdate
            })

        })

    },
    eliminarReciboDetalle: (req, res) => {
        var id_detalle = req.params.id_detalle;
        ReciboDetalle.findOneAndDelete({ _id: id_detalle }, (err, reciboRemove) => {
            if (err) {
                return res.status(200).send({
                        status: 'error',
                        menssaje: 'error al borrar'
                })
            }
            if (!reciboRemove) {
                    return res.status(200).send({
                        status: 'error',
                        menssaje: 'No se ha borrado el recibo Detalle, posiblemente no exista'
                    })
            }
            return res.status(200).send({
                status: 'success',
                cliente: reciboRemove
            })
        })
    },

    obtenerReciboDetalleByReciboCabId: (req, res) => {
        var id_cabecera = req.params.id_cabecera;
        var query = ReciboDetalle.find({ recibo_cabecera : id_cabecera}).populate('recibo_cabecera').populate('tipo_recibo');
        query.sort('-_id').exec((err, reciboDetalle) => {
            if (err) {

                return res.status(200).send({
                    status: 'error',
                    menssaje: 'errro al devolver los datos'+err
                })

            }
            if (!reciboDetalle) {

                return res.status(200).send({
                    status: 'error',
                    menssaje: 'no hay recibo detalle  para mostrar'
                })


            }

            return res.status(200).send({
                error: 'success',
                reciboDetalle
            })

        })
    },

}

module.exports = controller