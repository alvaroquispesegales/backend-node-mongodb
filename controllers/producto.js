'use strict'
var validator = require('validator');
var fs = require('fs');
var path = require('path');
var Producto = require('../models/producto');


var controller = {

    guardarProducto: (req, res) => {
        var params = req.body
        try {

            var validate_codigo_barra = !validator.isEmpty(params.codigo_barra);
            var validate_codigo = !validator.isEmpty(params.codigo);
            var validate_categoria = !validator.isEmpty(params.categoria_id);
            var validate_nombre = !validator.isEmpty(params.nombre);
            var validate_precio = !validator.isEmpty(params.precio);
            var validate_stock = !validator.isEmpty(params.stock);
            var validate_descripcion = !validator.isEmpty(params.descripcion);

        } catch (err) {
            return res.status(200).send({
                status: 'error',
                menssaje: 'fatan datos por enviar'
            })
        }
        if (validate_nombre &&
            validate_descripcion &&
            validate_precio &&
            validate_stock &&
            validate_categoria &&
            validate_codigo_barra &&
            validate_codigo) {

            //asignar valores
            var producto = new Producto();
            producto.nombre = params.nombre;
            producto.precio = params.precio;
            producto.descripcion = params.descripcion;
            producto.stock = params.stock;
            producto.image = null;
            producto.categoria = params.categoria_id;
            producto.codigo_barra = params.codigo_barra;
            producto.codigo = params.codigo;
            //guardar el articulo
            producto.save((err, productoStore) => {
                if (err || !productoStore) {
                    setTimeout(function () {
                        return res.status(200).send({
                            status: 'error',
                            menssaje: 'el articulo no se ha guardado'
                        })
                    }, 2000);

                }
            });
            //devolver una respuesta
            setTimeout(function () {
                return res.status(200).send({
                    status: 'success',
                    producto
                })
            }, 2000);

        } else {


            setTimeout(function () {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'los datos no son validos'
                })
            }, 2000);

        }
    },

    obtenerProductos: (req, res) => {
        var query = Producto.find({}).populate('categoria');

        var last = req.params.last;
        if (last || last != undefined) {
            query.limit(5);
        }
        query.sort('-_id').exec((err, productos) => {
            if (err) {


                return res.status(200).send({
                    status: 'error',
                    menssaje: 'errro al devolver los datos'
                })


            }
            if (!productos) {


                return res.status(200).send({
                    status: 'error',
                    menssaje: 'no hay productos para mostrar'
                })



            }




            var resultado = productos.map(function (productos) {
                var rObj = {
                    _id: productos._id,
                    nombre: productos.nombre,
                    precio: productos.precio,
                    descripcion: productos.descripcion,
                    stock: productos.stock,
                    image: productos.image,
                    categoria: productos.categoria? productos.categoria.nombre:'',
                    categoria_id: productos.categoria? productos.categoria._id:0,
                    codigo: productos.codigo,
                    codigo_barra: productos.codigo_barra
                };

                return rObj;
            });

            return res.status(200).send({
                status: 'success',
                productos: resultado
            })


        })

    },
    obtenerProducto: (req, res) => {
        var productoId = req.params.id;
        if (!productoId && productoId == null) {
            return res.status(200).send({
                status: 'error',
                menssaje: 'No exisate el articulo'
            })
        }

        Producto.findById(productoId, (err, producto) => {

            if (err || !producto) {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'No exisate el producto'
                })
            }
            return res.status(200).send({
                status: 'success',
                producto
            })
        })
    },
    actualizarProducto: (req, res) => {


        var productoId = req.params.id;
        var params = req.body

        try {
            var validate_codigo_barra = !validator.isEmpty(params.codigo_barra);
            var validate_codigo = !validator.isEmpty(params.codigo);
            var validate_categoria = !validator.isEmpty(params.categoria_id);
            var validate_nombre = !validator.isEmpty(params.nombre);
            /*var validate_precio = !validator.isEmpty(params.precio);
            var validate_stock = !validator.isEmpty(params.stock);*/
            var validate_descripcion = !validator.isEmpty(params.descripcion);
        }
        catch (err) {
            return res.status(200).send({
                status: 'error',
                menssaje: 'Faltan datos por enviar' + err
            })
        }

        if (validate_nombre && validate_descripcion &&
            validate_codigo_barra && validate_codigo && validate_categoria) {
            params.categoria = params.categoria_id;
            Producto.findByIdAndUpdate({ _id: productoId }, params, { new: true }, (err, productoUpdate) => {
                if (err) {

                    setTimeout(function () {
                        return res.status(200).send({
                            status: 'error',
                            menssaje: 'Error al actualizar: ' + err
                        })

                    }, 2000);

                }
                if (!productoUpdate) {


                    setTimeout(function () {
                        return res.status(200).send({
                            status: 'error',
                            menssaje: 'No existe el cliente'
                        })
                    }, 2000);

                }


                setTimeout(function () {
                    return res.status(200).send({
                        status: 'success',
                        producto: productoUpdate
                    })
                }, 2000);
            })
        } else {
            return res.status(200).send({
                status: 'error',
                menssaje: 'La validacion no es correcta'
            })
        }
    },
    eliminarProducto: (req, res) => {
        var productoId = req.params.id;
        Producto.findOneAndDelete({ _id: productoId }, (err, productoRemoved) => {
            if (err) {




                setTimeout(function () {
                    return res.status(200).send({
                        status: 'error',
                        menssaje: 'error al borrar'
                    })
                }, 2000);

            }
            if (!productoRemoved) {



                setTimeout(function () {
                    return res.status(200).send({
                        status: 'error',
                        menssaje: 'No se ha borrado el producto, posiblemente no exista'
                    })
                }, 2000);

            }


            setTimeout(function () {
                return res.status(200).send({
                    status: 'success',
                    producto: productoRemoved
                })

            }, 2000);

        })
    },
    cargarImagen: (req, res) => {
        var file_name = 'Imagen no subida...';
        if (!req.files) {
            return res.status(200).send({
                status: 'error',
                menssaje: file_name
            })
        }

        var file_path = req.files.file0.path;
        var file_split = file_path.split('\\');
        var file_name = file_split[2];

        var extencion_split = file_name.split('\.');
        var file_ext = extencion_split[1];
        /*if(file_ext !='png' && file_ext !='jpg' && file_ext !='jpeg' && file_ext !='gif'){
            //borrara el archivo subido
            fs.unlink(file_path,(err)=>{
                return res.status(200).send({
                    status : 'error',
                    menssaje : 'La extencion de la imgen no es valida'
                })
            })
        }else{*/
        var productoId = req.params.id;
        Producto.findOneAndUpdate({ _id: productoId }, { image: file_name }, { new: true }, (err, productoUpdate) => {
            if (err || !productoUpdate) {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'Error al guardar la imagen del producto: ' + err
                })
            }
            return res.status(200).send({
                status: 'success',
                producto: productoUpdate
            })
        });
        //}

    },

    obtenerImage: (req, res) => {

        var file = req.params.image;
        var path_file = './upload/productos/' + file;
        fs.exists(path_file, (exists) => {

            if (exists) {
                return res.sendFile(path.resolve(path_file));
            } else {
                return res.sendFile(path.resolve('./upload/productos/img-no-diponible.png'));
            }
        })

    }

};

module.exports = controller