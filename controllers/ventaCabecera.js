'use strict'
var validator = require('validator');
var fs = require('fs');
var VentaCabecera = require('../models/ventaCabecera');
var controller = {
    guardarVentaCabecera: (req, res) => {
        var params = req.body

        
        try {
            var validate_nro_identificacion = !validator.isEmpty(params.nro_identificacion);
            var validate_nro_venta = !validator.isEmpty(params.nro_venta);
            var validate_cliente = !validator.isEmpty(params.cliente);
            var validate_comprobante = !validator.isEmpty(params.comprobante);

        } catch (err) {
            return res.status(200).send({
                status: 'error',
                menssaje: 'fatan datos por enviar'
            })
        }
        if (validate_nro_venta && validate_nro_identificacion && validate_cliente && validate_comprobante) {

            //asignar valores
            var ventaCab = new VentaCabecera();
            ventaCab.nro_identificacion = params.nro_identificacion;
            ventaCab.nro_venta = params.nro_venta;
            ventaCab.cliente = params.cliente;
            ventaCab.comprobante = params.comprobante;
            ventaCab.nota = params.nota;
            ventaCab.fecha_venta = params.fecha_venta;
            //guardar la venta cabecera
            ventaCab.save((err, ventaCabStore) => {
                if (err || !ventaCabStore) {
                    return res.status(200).send({
                        status: 'error',
                        menssaje: 'la venta no se ha guardado'
                    })
                }
                setTimeout(function () {
                    return res.status(200).send({
                        status: 'success',
                        ventaCabecera: ventaCabStore
                    })
                }, 2000);
            });

        } else {
            return res.status(200).send({
                status: 'error',
                menssaje: 'los datos no son validos'
            })
        }
    },
    obtenerVentaCabecera: (req, res) => {
        var query = VentaCabecera.find({}).populate('cliente').populate('comprobante')
        query.sort('-_id').exec((err, ventasCabecera) => {
            if (err) {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'errro al devolver los datos'
                })
            }
            if (!ventasCabecera) {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'no hay ventas para mostrar'
                })
            }
            var resultado = ventasCabecera.map(function(obj){ 
                var rObj = {
                    _id : obj._id,
                    nro_identificacion:obj.nro_identificacion,
                    nro_venta:obj.nro_venta,
                    cliente:(obj.cliente)?obj.cliente.nombre_completo:'',
                    comprobante:obj.comprobante?obj.comprobante.nombre:'',
                    nota:obj.nota,
                    total:123,
                    vendedor:"Carla Quisbert"
                };
                return rObj;
             });


            return res.status(200).send({
                error: 'success',
                ventasCabecera:resultado
            })
        })
    },
}

module.exports = controller