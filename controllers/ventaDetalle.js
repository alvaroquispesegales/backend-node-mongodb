'use strict'
var validator = require('validator');
var fs = require('fs');
var VentaDetalle = require('../models/ventaDetalle');
var controller = {
    guardarVentaDetalle: (req, res) => {
        var params = req.body
        try {
            var validate_venta_cabecera = !validator.isEmpty(params.venta_cabecera);

        } catch (err) {
            return res.status(200).send({
                status: 'error',
                menssaje: 'fatan datos por enviar'
            })
        }
        if (validate_venta_cabecera && params.productos.length > 0) {

            params.productos.forEach(function (item) {
                var ventaDet = new VentaDetalle();
                ventaDet.venta_cabecera = params.venta_cabecera;
                ventaDet.producto = item._id;
                ventaDet.save((err, ventaDetStore) => {
                    if (err || !ventaDetStore) {
                        return res.status(200).send({
                            status: 'error',
                            menssaje: 'la venta detalle no se ha guardado'
                        })
                    }
     
                });
            }, this);
            setTimeout(function () {
                return res.status(200).send({
                    status: 'success',
                    menssaje: 'Actualizacion de Datos Corecto'
                })
            }, 2000);

        } else {
            return res.status(200).send({
                status: 'error',
                menssaje: 'los datos no son validos'
            })
        }
    },
    obtenerVentaDetalle: (req, res) => {

        var query = VentaDetalle.find({ venta_cabecera: req.params.id_cabecera }).populate('venta_cabecera').populate('producto')
        query.sort('-_id').exec((err, ventasDetalle) => {
            if (err) {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'errro al devolver los datos'
                })
            }
            if (!ventasDetalle) {
                return res.status(200).send({
                    status: 'error',
                    menssaje: 'no hay ventas detalle para mostrar'
                })
            }
            return res.status(200).send({
                error: 'success',
                ventasDetalle
            })
        })
    },
}

module.exports = controller