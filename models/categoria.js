'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var CategoriaSchema = Schema({
    nombre:String,
    nota:String
});

module.exports = mongoose.model('Categoria',CategoriaSchema) 