'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ClienteSchema = Schema({
    identificacion:String,
    nombre_completo:String,
    correo : String,
    telefono:String,
    direccion:String,
    nota:String

});

module.exports = mongoose.model('Cliente',ClienteSchema) 