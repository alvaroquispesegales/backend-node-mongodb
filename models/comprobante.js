'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ComprobanteSchema = Schema({
    nombre:String,
    nota:String
});
module.exports = mongoose.model('Comprobante',ComprobanteSchema) 