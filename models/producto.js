'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ProductoSchema = Schema({
    codigo_barra:String,
    codigo:String,
    categoria:{ type: Schema.ObjectId, ref: "Categoria" },
    nombre:String,
    precio:Number,
    stock:Number,
    descripcion : String,
    image:String

});

module.exports = mongoose.model('Producto',ProductoSchema) 