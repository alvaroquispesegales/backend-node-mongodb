'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ReciboCabeceraSchema = Schema({
    nro_recibo:String,
    cliente:{ type: Schema.ObjectId, ref: "Cliente" },
    fecha_recibo: {
        type: Date,
        default: Date.now
    },
    nota:String
   
});

module.exports = mongoose.model('ReciboCabecera',ReciboCabeceraSchema) 