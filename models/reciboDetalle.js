'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ReciboDetalleSchema = Schema({
    recibo_cabecera:{ type: Schema.ObjectId, ref: "ReciboCabecera" },
    tipo_recibo:{ type: Schema.ObjectId, ref: "TipoRecibo" },
    valor:Number,
    nota:String
});

module.exports = mongoose.model('ReciboDetalle',ReciboDetalleSchema) 