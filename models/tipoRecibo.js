'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var TipoReciboSchema = Schema({
    nombre:String,
    nota:String
});
module.exports = mongoose.model('TipoRecibo',TipoReciboSchema) 
