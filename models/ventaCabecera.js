'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var VentaCabeceraSchema = Schema({
    nro_identificacion:String,
    nro_venta:String,
    cliente:{ type: Schema.ObjectId, ref: "Cliente" },
    comprobante:{ type: Schema.ObjectId, ref: "Comprobante" },
    nota:String,
    fecha_venta: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('VentaCabecera',VentaCabeceraSchema) 