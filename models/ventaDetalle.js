'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var VentaDetalleSchema = Schema({
    venta_cabecera:{ type: Schema.ObjectId, ref: "VentaCabecera" },
    producto:{ type: Schema.ObjectId, ref: "Producto" }
});

module.exports = mongoose.model('VentaDetalle',VentaDetalleSchema) 