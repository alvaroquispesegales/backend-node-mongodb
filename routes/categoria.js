'use estrict'
var express = require('express');
var CategoriaController = require('../controllers/categoria');
var router = express.Router();


//rutas utiles
router.post('/guardarCategoria',CategoriaController.guardarCategoria);
router.get('/obtenerCategorias',CategoriaController.obtenerCategorias);

router.put('/actualizarCategoria/:id',CategoriaController.actualizarCategoria);
router.delete('/eliminarCategoria/:id',CategoriaController.eliminarCategoria);

module.exports = router;