'use estrict'
var express = require('express');
var ClienteControler = require('../controllers/cliente');
var router = express.Router();


//rutas utiles
router.post('/guardarCliente',ClienteControler.guardarCliente);
router.get('/obtenerClientes/:last?',ClienteControler.obtenerClientes);
router.get('/obtenerCliente/:id',ClienteControler.obtenerCliente);
router.put('/actualizarCliente/:id',ClienteControler.actualizarCliente);
router.delete('/eliminarCliente/:id',ClienteControler.eliminarCliente);

module.exports = router;