'use estrict'
var express = require('express');
var InformeCuentaCorrienteController = require('../controllers/cuentas-por-cobrar/informe-cuenta-corriente');
var ReciboController = require('../controllers/cuentas-por-cobrar/recibo-cabecera');
var ReciboDetController = require('../controllers/cuentas-por-cobrar/recibo-detalle');
var router = express.Router();


//rutas de cabecera
router.get('/obtenerCuentaCorriente/:id_cliente',InformeCuentaCorrienteController.obtenerCuentaCorriente)
router.post('/guardarReciboCabecera',ReciboController.guardarReciboCabecera);
router.get('/obtenerReciboDetalleByReciboCabId/:id_cabecera',ReciboDetController.obtenerReciboDetalleByReciboCabId);
router.put('/actualizarReciboCabecera/:id_cabecera',ReciboController.actualizarReciboCabecera);

//rutas de etalle
router.post('/guardarReciboDetalle',ReciboDetController.guardarReciboDetalle);
router.put('/actualizarReciboDetalle/:id_detalle',ReciboDetController.actualizarReciboDetalle);
router.delete('/eliminarReciboDetalle/:id_detalle',ReciboDetController.eliminarReciboDetalle);


module.exports = router;