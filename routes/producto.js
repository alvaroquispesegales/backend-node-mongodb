'use estrict'
var express = require('express');
var ProductoControllers = require('../controllers/producto');
var router = express.Router();

var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir:'./upload/productos'})

//rutas utiles
router.post('/guardarProductos',ProductoControllers.guardarProducto);
router.get('/obtenerProductos/:last?',ProductoControllers.obtenerProductos);
router.get('/obtenerProducto/:id',ProductoControllers.obtenerProducto);
router.put('/actualizarProducto/:id',ProductoControllers.actualizarProducto);
router.delete('/eliminarProducto/:id',ProductoControllers.eliminarProducto);
router.post('/cargarImagen/:id', md_upload, ProductoControllers.cargarImagen);
router.get('/obtenerImagen/:image',ProductoControllers.obtenerImage);


module.exports = router;