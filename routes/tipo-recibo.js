'use estrict'
var express = require('express');
var TipoRecibo = require('../controllers/cuentas-por-cobrar/tipo_recibo');
var router = express.Router();

router.get('/obtenerTipoRecibos',TipoRecibo.obtenerTipoRecibos);
router.post('/guardarTipoRecibo',TipoRecibo.guardarTipoRecibo);

module.exports = router;