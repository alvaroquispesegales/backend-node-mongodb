'use estrict'
var express = require('express');
var ventaCabeceraControllers = require('../controllers/ventaCabecera');
var ventaDetalleController = require('../controllers/ventaDetalle');
var router = express.Router();

//rutas utiles
router.post('/guardarVentaCabecera',ventaCabeceraControllers.guardarVentaCabecera);
router.get('/obtenerVentaCabecera',ventaCabeceraControllers.obtenerVentaCabecera);

router.post('/guardarVentaDetalle',ventaDetalleController.guardarVentaDetalle);
router.get('/obtenerVentaDetalle/:id_cabecera',ventaDetalleController.obtenerVentaDetalle);

module.exports = router;